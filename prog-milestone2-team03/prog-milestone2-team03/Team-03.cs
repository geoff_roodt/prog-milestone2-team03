﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team03
{
    class Program
    {
        // Global dictionary to hold score and number of games played.
        public static Dictionary<int, int> GamesPlayed = new Dictionary<int, int>();
        
        static void Main(string[] args)
        {
            var endApp = false;

            do
            {
                try
                {
                    var userSelection = 0;
                    var userIn = string.Empty;
                    userSelection = GetUserSelection(ref endApp);

                    switch (userSelection)
                    {
                        case 1:
                            var todaysDate = DateTime.Now;
                            TotalDaysOld(todaysDate);
                            TotalDaysPassed(todaysDate);
                            break;

                        case 2:
                            CodyMain();
                            break;

                        case 3:
                            GrantsMain();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    var dispLine = new string('*', 60);
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine(dispLine);
                    Console.WriteLine(dispLine);
                    Console.WriteLine("A Error Occured, You Have Been Directed Back to the Main Menu");
                    Console.WriteLine(dispLine);
                    Console.WriteLine(dispLine);
                    Console.WriteLine();
                    Console.WriteLine();
                }

            } while (!endApp);

            Console.WriteLine();
            Console.WriteLine("You have ended the programme!");

            Console.ReadLine();            
        }

        /// <summary>
        /// Displays the main menu
        /// </summary>
        private static void DisplayMenu()
        {
            var dispLine = new String('*', 60);
            Console.WriteLine(dispLine);
            Console.WriteLine("Welcome to Team 3's Application!");
            Console.WriteLine();
            Console.WriteLine("Please Enter 1, 2, 3 to do the appropriate task");
            Console.WriteLine("    Or press 'enter' to end the application");
            Console.WriteLine(dispLine);
            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine("(1) Date");
            Console.WriteLine("(2) Grade Average");
            Console.WriteLine("(3) Random Number Generator");
        }

        /// <summary>
        /// Gets the user's selection for the task they want to complete
        /// </summary>
        /// <param name="endApp">Variable to represent the end game state</param>
        /// <returns>The user selection as integer based</returns>
        private static int GetUserSelection(ref bool endApp)
        {
            var selection = 0;
            var endSelection = false;
            var userIn = string.Empty;
            var dispLine = new String('*', 60);

            endApp = false;
            Console.WriteLine(dispLine);
            DisplayMenu();

            do
            {
                Console.WriteLine(dispLine);
                userIn = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(userIn) || userIn.ToUpper() == "END")
                {
                    endApp = true;
                    break;
                }
                else if (int.TryParse(userIn, out selection))
                {
                    if (selection >= 1 && selection <= 3)
                    {
                        endSelection = true;
                    }
                    else
                    {
                        Console.WriteLine("Please Enter a Valid Selection");
                    }
                }
                else if (!string.IsNullOrEmpty(userIn))
                {
                    Console.WriteLine("Please Enter a Valid Selection");
                }

            } while (!endSelection);

            return selection;
        }

        /// <summary>
        /// Calculate how many days old the user from their birthday
        /// </summary>
        /// <param name="today"></param>
        private static void TotalDaysOld(DateTime today)
        {
            var dispLine = new string('*', 75);
            var userAge = DateTime.MinValue;
            var validDate = false;

            Console.WriteLine();
            Console.WriteLine(dispLine);
            do
            {
                Console.WriteLine("Enter when you were born: dd/MM/yyy | 1 January 1869 | yyyy/MM/dd");
                if (DateTime.TryParse(Console.ReadLine(), out userAge) && userAge < today)
                {
                    var totalDays = Math.Round((today - userAge).TotalDays);
                    Console.WriteLine($"You are {totalDays} days old!");

                    validDate = true;                    
                }
                else
                {
                    Console.WriteLine("Invalid Entry!");
                    Console.WriteLine(dispLine);
                    Console.WriteLine();
                }

            } while (!validDate);
        }

        /// <summary>
        /// Calculate how many days there are between now and a future year
        /// </summary>
        /// <param name="today"></param>
        private static void TotalDaysPassed(DateTime today)
        {
            var numofYears = 0;
            var validEntry = false;
            var dispLine = new string('*', 75);

            Console.WriteLine();
            while (!validEntry)
            {
                Console.WriteLine("Enter a number of years to see how many days are in them!");
                if (int.TryParse(Console.ReadLine(), out numofYears))
                {
                    var futureDate = today.AddYears(numofYears);
                    var totalDays = Math.Round((futureDate - today).TotalDays);

                    Console.WriteLine($"There are {totalDays} days in {numofYears} Years");
                    validEntry = true;
                }
                else
                {
                    Console.WriteLine("Invalid Entry!");
                }
            }                    
        }

        /// <summary>
        /// Method to run the number generator "game" and loop until user chooses to return to the main menu
        /// </summary>
        private static void GrantsMain()
        {
            Random numberGenerator = new Random();
            GamesPlayed.Clear();

            var playAgain = true;
            var numofGames = 0;
            var score = 0;
            var dispLine = new string('*', 60);
            var userInput = 0;

            while (playAgain)
            {

                for (int i = 0; i < 5; i++)
                {
                    gameStart:
                    Console.WriteLine(dispLine);
                    Console.WriteLine("This is a random number generator game.");
                    Console.WriteLine("Please enter a number between 1 and 5: ");
                    Console.WriteLine();

                    if (!int.TryParse(Console.ReadLine(), out userInput))
                    {
                        Console.WriteLine("That is an invalid entry, please try again.");
                        Console.WriteLine();
                        goto gameStart;
                    }

                    int number = numberGenerator.Next(1, 6);

                    if (userInput == number)
                    {
                        Console.WriteLine();
                        Console.WriteLine("You guessed correctly.");
                        Console.WriteLine();
                        score++;
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("Incorrect. Please try again.");
                        Console.WriteLine();
                    }
                }

                Console.WriteLine($"Congratulations, you got: {score} points");

                numofGames++;
                playAgain = CanUserPlayAgain(numofGames, score);
            }

            Console.WriteLine(dispLine);
            Console.WriteLine("Press any key to return to the main menu: ");
            Console.ReadLine();
            Console.WriteLine();
        }

        /// <summary>
        /// Method to check if the user wants to play another round
        /// </summary>
        /// <param name="Score">The score of the user</param>
        /// <param name="numofGames">The amount of games user has played</param>
        /// <returns>Return the user's choice</returns>
        private static bool CanUserPlayAgain(int numofGames, int score)
        {
            var dispLine = new string('*', 60);
            var y = "y";
            var n = "n";
            var userInput = "";
            var playAgain = true;
            var validEntry = false;

            GamesPlayed.Add(numofGames, score);

            while (!validEntry)
            {
                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine();
                Console.WriteLine("Do you wish to play again? Type Y/N: ");
                userInput = Console.ReadLine();
                Console.WriteLine();
                Console.WriteLine(dispLine);

                if (userInput.ToLower() == y)
                {
                    playAgain = true;
                    validEntry = true;
                }
                else if (userInput.ToLower() == n)
                {
                    playAgain = false;
                    validEntry = true;
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Your entry was invalid.");
                }
            }

            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine($"Number of games played: {numofGames}");
            Console.WriteLine();

            for (int i = 0; i < GamesPlayed.Count; i++)
            {
                Console.WriteLine($"For game {GamesPlayed.ElementAt(i).Key} you scored {GamesPlayed.ElementAt(i).Value}");
            }

            Console.WriteLine();
            Console.WriteLine(dispLine);

            return playAgain;


        }

        /// <summary>
        /// Method to run the Grade Averages task
        /// </summary>
        static void CodyMain()
        {
            var dispLine = new String('*', 60);
            string studentID = "0";
            int userInput = 0;
            var A1List = new List<string>();

            studentID = Identification();
            int level = levels();
            var classes = Papers(level);

            do
            {
                
                Console.WriteLine(dispLine);
                Console.WriteLine();
                Console.WriteLine("Press 1 to see your student ID, grades and your level of study");
                Console.WriteLine("Press 2 to see your average of all your marks");
                Console.WriteLine("Press 3 to see all papers with an A+ grade");
                Console.WriteLine("Press 4 to return to the main menu");
                userInput = int.Parse(Console.ReadLine());

                if (userInput == 1)
                {
                    A1List = Results(classes, studentID, level);
                    userInput = 0;
                }
                else if (userInput == 2)
                {
                    Averages(classes);
                    userInput = 0;
                }
                else if (userInput == 3)
                {
                    aPlus(A1List);
                    userInput = 0;
                }
                else if (userInput == 4)
                {

                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Please eneter a valid number");
                    userInput = 0;
                }
            } while (userInput == 0);

        }

        /// <summary>
        /// Return the level the user is studying at
        /// </summary>
        /// <returns></returns>
        static int levels()
        {
            int level = 0;
            Console.WriteLine();
            Console.WriteLine("Are you level 5 or 6?");

            level = int.Parse(Console.ReadLine());
            return level;
        }

        /// <summary>
        /// Get the papers the user is studying and the marks for the paper
        /// </summary>
        /// <param name="level">The users level</param>
        /// <returns>A dictionary filled with papers and marks</returns>
        static Dictionary<string, double> Papers(int level)
        {
           
            int papers = 0;
            var validEntry = false;
            var classes = new Dictionary<string, double>();
            string userInput = "0";
            double percentage = 0;
            var dispLine = new String('*', 60);

            while (!validEntry)
            {

                if (level == 5)
                {
                    papers = papers + 4;
                    validEntry = true;
                }
                else if (level == 6)
                {
                    papers = papers + 3;
                    validEntry = true;
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Error, please enter a valid level (5 or 6)");
                }

            }
                       
            if (papers == 4)
            {
                for (int i = 1; i < 5; i++)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Enter your paper code number {i}");
                    userInput = Console.ReadLine();

                    Console.WriteLine();
                    Console.WriteLine("Now the percentage you got on that paper");
                    percentage = double.Parse(Console.ReadLine());

                    classes.Add(userInput, percentage);
                }
            }
            else
            {
                for (int i = 1; i < 4; i++)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Enter your paper code number {i}");
                    userInput = Console.ReadLine();

                    Console.WriteLine();
                    Console.WriteLine("Now the percentage you got on that paper");
                    percentage = double.Parse(Console.ReadLine());

                    classes.Add(userInput, percentage);
                }
            }
            

            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine("These are your papers and their grades:");

            for (int i = 0; i < classes.Count; i++)
            {
                Console.WriteLine($"{classes.ElementAt(i)}%");
            }

            return classes;
        }

        /// <summary>
        /// Get the studen ID
        /// </summary>
        /// <returns>The student ID</returns>
        static string Identification()
        {
            string studentID = "0";

            Console.WriteLine();
            Console.WriteLine("Please enter your student ID");
            studentID = Console.ReadLine();

            return studentID;

        }

        /// <summary>
        /// Sorts the marks for papers and displays the the papers with A+ mark
        /// </summary>
        /// <param name="classes">The papers dictionary</param>
        /// <param name="studentID">The students id</param>
        /// <param name="level">the students level</param>
        /// <returns>A list of papers that have A+ mark</returns>
        static List<string> Results(Dictionary<string, double> classes, string studentID, int level)
        {
            var A1List = new List<string>(); 
            var AList = new List<string>(); 
            var AminusList = new List<string>(); 
            var BPLusList = new List<string>(); 
            var BList = new List<string>(); 
            var BMinusList = new List<string>(); 
            var CPLusList = new List<string>(); 
            var CList = new List<string>(); 
            var DList = new List<string>();  
            var EList = new List<string>();
            var dispLine = new String('*', 60);

            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine($"Your ID: {studentID}");
            Console.WriteLine($"Your Level of study: {level}");
            Console.WriteLine($"Your Papers and Grades:");

            for (int i = 0; i < classes.Count; i++)
            {
                Console.WriteLine($"{classes.ElementAt(i)}%");

            }

            foreach (var x in classes)
            {

                if (x.Value >= 85 && x.Value <= 100)
                {
                    A1List.Add(x.Key);
                }

                if (x.Value >= 80 && x.Value <= 84)
                {
                    AList.Add(x.Key);
                }

                if (x.Value >= 75 && x.Value <= 79)
                {
                    AminusList.Add(x.Key);
                }

                if (x.Value >= 70 && x.Value <= 74)
                {
                    BPLusList.Add(x.Key);
                }

                if (x.Value >= 65 && x.Value <= 69)
                {
                    BList.Add(x.Key);
                }

                if (x.Value >= 60 && x.Value <= 64)
                {
                    BMinusList.Add(x.Key);
                }

                if (x.Value >= 55 && x.Value <= 59)
                {
                    CPLusList.Add(x.Key);
                }

                if (x.Value >= 50 && x.Value <= 54)
                {
                    CList.Add(x.Key);
                }

                if (x.Value >= 40 && x.Value <= 49)
                {
                    DList.Add(x.Key);
                    
                }

                if (x.Value >= 0 && x.Value <= 39)
                {
                    EList.Add(x.Key);                          
                }

            }

            if (A1List.Count > 0)
            {
                Console.WriteLine($"A+: { string.Join(", ", A1List)}");
            }
            if (AList.Count > 0)
            {
                Console.WriteLine($"A: { string.Join(", ", AList)}");
            }
            if (AminusList.Count > 0)
            {
                Console.WriteLine($"A-: { string.Join(", ", AminusList)}");
            }
            if (BPLusList.Count > 0)
            {
                Console.WriteLine($"B+: { string.Join(", ", BPLusList)}");
            }
            if (BList.Count > 0)
            {
                Console.WriteLine($"B: { string.Join(", ", BList)}");
            }
            if (BMinusList.Count > 0)
            {
                Console.WriteLine($"B-: { string.Join(", ", BMinusList)}");
            }
            if (CPLusList.Count > 0)
            {
                Console.WriteLine($"C+: { string.Join(", ", CPLusList)}");
            }
            if (CList.Count > 0)
            {
                Console.WriteLine($"C: { string.Join(", ", CList)}");
            }
            if (DList.Count > 0)
            {
                Console.WriteLine($"D: { string.Join(", ", DList)}");
            }
            if (DList.Count > 0)
            {
                Console.WriteLine($"E: { string.Join(", ", EList)}");
            }


            return A1List;

        }

        /// <summary>
        /// Gets the average mark for the papers
        /// </summary>
        /// <param name="classes">Dictionary containing the marks</param>
        static void Averages(Dictionary<string, double> classes) 
        {
            var resultslist = new List<double>();
            double mean = 0;
            var dispLine = new String('*', 60);

            foreach (var x in classes)
            {

                if (x.Value >= 0 && x.Value <= 100)
                {
                    resultslist.Add(x.Value);
                }
            }
            
            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine($"Your results are: { string.Join(", ", resultslist)}");

            mean = resultslist.Average(num => Convert.ToInt64(num));
            Console.WriteLine();
            Console.WriteLine("The average is {0}%", mean);
        }

        /// <summary>
        /// Displays the list of papers with A+ mark
        /// </summary>
        /// <param name="A1List"></param>
        static void aPlus(List<string> A1List)
        {
            var dispLine = new String('*', 60);

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine($"These papers are graded A+ : { string.Join(", ", A1List)}");
        } 
    }
}
    

